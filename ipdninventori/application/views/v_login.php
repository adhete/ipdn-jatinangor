<?php
//echo form_open('Login/FormLogin');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo base_url()."assets/"; ?>/img/ipdn.png" type="image/gif">
<title>Login</title>

<link href="<?php echo base_url()."assets/"; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()."assets/"; ?>css/styles.css" rel="stylesheet">

<!--sweet alert-->
<link href="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
.parsley-error {
  border-color: #f05050 !important;
}
.parsley-errors-list {
  display: none;
  margin: 0;
  padding: 0;
}
.parsley-errors-list.filled {
  display: block;
}
.parsley-errors-list > li {
  font-size: 12px;
  list-style: none;
  color: #f6504d;
}
</style>

</head>

<body>
	<img style="display: block; margin: auto; width: 10%; padding-bottom: 10px" src="<?php echo base_url()."assets/"; ?>img/ipdn.png">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Silahkan Masuk</div>
				<div class="panel-body">
					<form action="<?php echo base_url(). 'index.php/Login/FormLogin'; ?>" method="post" data-parsley-validate novalidate>
						<fieldset>
						<div style="color: red">
					
						<?php
	                        $info=$this->session->flashdata('info');
	                        if(!empty($info))
	                        {
	                            echo $info;
	                        }
                    	?>
                    
                    	</div>
							<div class="form-group">
								<input class="form-control" type="text" name="username" required placeholder="pengguna">
							</div>
							<div class="form-group">
								<input class="form-control" type="password" name="password" required placeholder="sandi">
							</div>
							<button class="btn btn-primary" type="submit" name="submit">Masuk</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="<?php echo base_url()."assets/"; ?>js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url()."assets/"; ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()."assets/"; ?>js/parsley.min.js"></script>

	<!-- Sweet-Alert  -->
<script src="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/jquery.sweet-alert.init.js"></script>