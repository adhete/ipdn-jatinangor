<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				
			</div>
		</form>
		<ul class="nav menu">
			<li>
				<a href="<?php echo base_url();?>index.php/Dashboard">
					<svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg> 
				Dashboard</a>
			</li>

			<li  class="active">
				<a href="<?php echo base_url();?>index.php/Barang/data_barang">
					<svg class="glyph stroked pencil">
						<use xlink:href="#stroked-pencil"></use>
					</svg> 
				Barang Masuk

				</a>
			</li>

			<li>
				<a href="<?php echo base_url();?>index.php/BarangBaru">
					<svg class="glyph stroked clipboard with paper">
						<use xlink:href="#stroked-clipboard-with-paper"></use>
					</svg>
				Data Barang Baru
				<?php
					 $query = $this->db->query('SELECT kode_barang FROM barang WHERE kode_bagian="GDU"'); ?>
					<span class="badge badge-success"><?php echo $query->num_rows(); ?> </span>
					</a>
			</li>

			<li>
				<a href="<?php echo base_url();?>index.php/Merk">
					<svg class="glyph stroked calendar blank">
						<use xlink:href="#stroked-calendar-blank"></use>
					</svg> 
				Merk</a>
			</li>

			<li>
				<a href="<?php echo base_url();?>index.php/Kategori">
					<svg class="glyph stroked tag">
						<use xlink:href="#stroked-tag"></use>
					</svg> 
				Kategori</a>
			</li>

			<li>
				<a href="<?php echo base_url();?>index.php/Divisi">
					<svg class="glyph stroked table">
						<use xlink:href="#stroked-table"></use>
					</svg> Divisi / Bagian
				</a>
			</li>
		</ul>

	</div><!--/.sidebar-->