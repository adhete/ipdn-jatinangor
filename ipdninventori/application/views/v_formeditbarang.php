<?php echo form_open('Barang/proses_update'); ?>

									<div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tambah Barang</h4>
                                    </div>
                                    <div class="modal-body">

                                                <input type="hidden" name="id_barang" id="id_barang" value="<?php echo $barang->id_barang; ?>">
                                            

                                            <div class="form-group">
                                                <label>SNID/Serial</label>
                                                <input class="form-control" type="text" name="snid" value="<?php echo $barang->snid; ?>">
                                            </div>

                                          
                                            <div class='form-group'>
                                                <label>Kategori</label>
                                                    <select class='form-control' name="k_kategori" id='k_kategori'>
                                                        <option value="<?php echo $barang->id_kategori; ?>"><?php echo $barang->nama_kategori; ?></option>
                                                       <?php 
                                                       		$kategori=$this->db->get('kategori');
                                                            foreach ($kategori->result() as $kat) { ?>
                                                            <option value="<?php echo $kat->id_kategori; ?>">
                                                                <?php echo $kat->nama_kategori; ?>
                                                            </option>
                                                        <?php    }
                                                        ?>
                                                        
                                                </select>
                                            </div>

                                            <div class='form-group'>
                                                <label>Merk</label>
                                                    <select class='form-control' name="merk" id='merk'>
                                                        <option value="<?php echo $barang->id_merk; ?>"><?php echo $barang->nama_merk; ?></option>
                                                        <?php 
                                                       		$merk=$this->db->get('merk');
                                                            foreach ($merk->result() as $kat) { ?>
                                                            <option value="<?php echo $kat->id_merk; ?>">
                                                                <?php echo $kat->nama_merk; ?>
                                                            </option>
                                                        <?php    }
                                                        ?>
                                                    </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Nama/Tipe Barang</label>
                                                <input class="form-control" type="text" name="nama_barang" value="<?php echo $barang->nama_barang; ?>">
                                            </div>

                                        
                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <textarea class="form-control" name="keterangan"><?php echo $barang->keterangan; ?></textarea>
                                            </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="submit" name="submit">Ubah</button>
                                        <button type="button" onclick="refresh()" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                             

<?php echo form_close();?>
<script type="text/javascript">
	function refresh(){
		location.reload();
	}


</script>