<script type="text/javascript">
    var jml_data = 0;
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $judul ?></li>
            </ol>
        </div><!--/.row-->
        
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Barang</div>
                    <div class="panel-body">

                    
                   
                   
        <!--LOKASI-->
        <form id="kirim" method="post" onsubmit="return false">
            <div class="input-group">
            <label class="control-label" for="inputEmail">Ubah Lokasi</label>
            
            <select  class="form-control" style="width: 200px;" type="text" name="kode_bagian" id="kode_bagian">
                <?php foreach ($bagian as $row) { ?>
                <option value="<?php echo $row->kode_bagian; ?>">
                    <?php echo $row->nama_bagian; ?>
                </option>
                <?php } ?>
            </select>
            

            <div class="input-group-btn"> 
               
                    <button style="margin-right:1000px; margin-top:25px" onclick="simpanlokasi()"  class="btn btn-primary" name="simpan" id="simpan" data-placement="right">
                        Ubah</button>
               
            </div>
            </div>
        
        <!--END LOKASI-->
                    <span id="jml"></span>
                <table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th class="empty"></th>
                <th data-sortable="true">Kode Barang</th>
                <th data-sortable="true">Nama Barang</th>
                <th data-sortable="true">Kategori</th>
                <th data-sortable="true">Status</th>
                <th data-sortable="true">Lokasi</th>
                <th data-sortable="true">Ubah Status</th>
            </tr>
        </thead>
        <?php $i=1 ?>
        <tbody>
        <?php foreach ($daftarbarang as $lihat) { ?>
            <tr>
                <td width="30" class="empty">
                    <input onchange="addminjml('#ck_<?=$i?>')" id="ck_<?=$i?>" name="idbarang[]" type="checkbox" value="<?php echo $lihat->id_barang; ?>" >
                </td>
                <td><?php echo $lihat->kode_kategori; 
                          echo"/"; 
                          echo strtoupper(substr($lihat->nama_merk,0,3));
                          echo"/"; 
                          echo $lihat->snid;
                    ?>
                </td>
                <td><?php echo $lihat->nama_barang; ?></td>
                <td><?php echo $lihat->nama_kategori; ?></td>
                <td><?php echo '<div class="label label-danger"></i><strong>'.$lihat->status.'</strong></div>'?></td>
                <td><?php echo $lihat->nama_bagian; ?></td>
                <td>
                    <a class="btn btn-repair" data-toggle="modal" onclick="openModalRepair('<?php echo $lihat->id_barang; ?>')">
                        <div class="glyphicon glyphicon-wrench"></div> Perbaiki
                    </a>

                    <a class="btn btn-rusaktot" data-toggle="modal" onclick="openModalRusakTotal('<?php echo $lihat->id_barang; ?>')">
                        <div class="glyphicon glyphicon-remove-circle"></div> Rusak Total
                    </a>
                </td>
            </tr>
        <?php $i++;} ?>
              
        </tbody>
    </table>
    </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        
        
                                
            </div><!--/.col-->
        </div><!--/.row-->
    </div>  <!--/.main-->

    <!-- Trigger the modal with a button -->
                    
                    

                     <!-- Modal Repair -->
                        <div id="RepairModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Perbaiki Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        Apa anda yakin akan memperbaiki barang ini
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnRepair" class="btn btn-primary">Iya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Modal Repair -->

                    <!-- Modal Rusak Total -->
                        <div id="BrokenModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Perbaiki Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        Status Barang Akan Diubah jadi Rusak Total
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnBroken" class="btn btn-primary">Iya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Modal Rusak Total -->

                    <!-- modal message -->
                        <div id="modalMessage" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="msgHeader"></h4>
                                    </div>
                                    <div class="modal-body" id="msgMessage">
                                         
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="suksesAjak" class="btn btn-default" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- /modal message -->


                    



<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    function simpanlokasi()
                    {
                        var string = $("#kirim").serialize();
                        console.log(string+'&jml_data='+jml_data);
                        string += '&jml_data='+jml_data;
                        $.ajax({
                            type    : 'POST',
                            url     : '<?php echo site_url();?>/Status/simpanlokasi',
                            data    : string,
                            success : function(data){
                                if(jml_data<=0){
                                    alert("Pilih data terlebih dahulu");
                                } else {
                                    alert("Data Berhasil Diubah");
                                    location.reload();
                                }
                            }
                        });
                    }

                    function addminjml(idInput)
                    {
                        var isck = $(idInput+':checkbox:checked').length > 0;
                        if(isck) {
                            jml_data++;
                        } else {
                            if(jml_data>0) {
                                jml_data--;
                            }
                        }
                        //console.log('jml='+jml_data);
                    }


     //Ubah Barang Diperbaiki
    function openModalRepair(id_barang)
    {
        $("#btnRepair").attr('onclick',"updateRepair('"+id_barang+"')");
        $("#RepairModal").modal('show');
    }

    function updateRepair(id_barang)
    {
        //alert(id_barang);
        $("#RepairModal").modal('hide');
        
        url = "<?php echo base_url(); ?>index.php/Status/update";
        $.post(url, 
        {
            id_barang:id_barang
        }).done(function(data){
            var obj = JSON.parse(data);

            $("#msgHeader").html(obj.header);
            $("#msgMessage").html(obj.message);
            $("#modalMessage").modal('show');
            if(obj.header=="sukses") {
                $("#suksesAjak").attr('onclick',"redirekSukses()");
            }
        });
    }

    function redirekSukses()
    {
        location.reload();
    }
    //END Ubah Barang Diperbaiki

    //Ubah Barang Rusak Total
    function openModalRusakTotal(id_barang)
    {
        $("#btnBroken").attr('onclick',"updateBroken('"+id_barang+"')");
        $("#BrokenModal").modal('show');
    }

    function updateBroken(id_barang)
    {
        //alert(id_barang);
        $("#BrokenModal").modal('hide');
        
        url = "<?php echo base_url(); ?>index.php/Status/updateRusakTotal";
        $.post(url, 
        {
            id_barang:id_barang
        }).done(function(data){
            var obj = JSON.parse(data);

            $("#msgHeader").html(obj.header);
            $("#msgMessage").html(obj.message);
            $("#modalMessage").modal('show');
            if(obj.header=="Sukses") {
                $("#suksesAjak").attr('onclick',"redirekSuksesRusakTotal()");
            }
        });
    }

    function redirekSuksesRusakTotal()
    {
        location.reload();
    }
    //END Ubah Barang Rusak Total




        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
</script>