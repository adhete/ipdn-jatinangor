<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $judul ?></li>
            </ol>
        </div><!--/.row-->
        
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Barang</div>
                    <div class="panel-body">

                                   

                     <?php
                        $info=$this->session->flashdata('info');
                        if(!empty($info))
                        {
                            echo $info;
                        }
                    ?>

                    
                   
                    <!-- Trigger the modal with a button -->
                    
                        
                   <br>
                    <a class="btn btn-primary" data-toggle="modal" onclick="openModalTambah()">
                        <div class="glyphicon glyphicon-plus"></div> Tambah Barang
                    </a>

                    <a class="btn btn-primary" href="<?php echo base_url();?>index.php/CetakLaporan/export">
                        <div class="glyphicon glyphicon-print"></div> Export
                    </a>

                    
                <table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th data-sortable="true">No</th>
                <th data-sortable="true">Kode Barang</th>
                <th data-sortable="true">Nama Barang</th>
                <th data-sortable="true">Kategori</th>
                <th data-sortable="true">Status</th>
                
               
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <?php $i=1 ?>
        <tbody>
        <?php 
        //dump($daftarbarang);
         foreach ($daftarbarang->result() as $row) { 

            ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row->kode_kategori; 
                          echo"/"; 
                          echo strtoupper(substr($row->nama_merk,0,3));
                          echo"/"; 
                          echo (substr($row->snid,0,5));
                    ?>
                </td>
                <td><?php echo $row->nama_barang; ?></td>
                <td><?php echo $row->nama_kategori; ?></td>
                <td>
                    <?php
                    if($row->status =="Tersedia"){
                        echo '<div class="label label-success"><strong>'.$row->status.'</strong></div>';
                        }else if($row->status =="Terpakai") {
                        echo '<div class="label label-primary"></i><strong>'.$row->status.'</strong></div>';
                        }else if($row->status =="Diperbaiki") {
                        echo '<div class="label label-warning"></i><strong>'.$row->status.'</strong></div>';
                        }else if($row->status =="Rusak"){
                        echo '<div class="label label-danger"></i><strong>'.$row->status.'</strong></div>';
                        }else {
                        echo '<div class="label label-info"></i><strong>'.$row->status.'</strong></div>';
                        };
                    ?>
                </td>
                
                <td>

                    <a class="btn btn-primary" href="<?php echo base_url();?>index.php/Barang/detail/<?php echo $row->id_barang; ?>"> 
                     <div class="glyphicon glyphicon-list-alt"></div>
                       Detail 
                    </a>

                    <a class="btn btn-primary"data-toggle="modal" data-target="#EditModal" href="<?php echo base_url(); ?>index.php/Barang/ubah/<?php echo $row->id_barang; ?>">
                        <div class="glyphicon glyphicon-edit"></div> Ubah</a>

                    <a class="btn btn-danger" onclick="openModalHapus('<?php echo $row->id_barang; ?>')">
                        <div class="glyphicon glyphicon-trash"></div> Hapus
                    </a>
                </td>
            </tr>
        <?php } ?>
              
        </tbody>
    </table>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        
        
                                
            </div><!--/.col-->
        </div><!--/.row-->
    </div>  <!--/.main-->

    <!-- Trigger the modal with a button -->
                    
                    

                    <!-- EditModal -->
                        <div id="EditModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                </div>
                            </div>
                        </div>
                
                    <!-- END EditModal -->
<?php foreach ($daftarbarang as $barang) { ?>
  

<div id="UpdateModal" class="modal fade" role="dialog">
<div class="modal-dialog">
                        <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Kategori</h4>
    </div>

    <div class="modal-body">
        <div class="form-group">
            <label>Kode Barang</label>
            <input class="form-control" name="kode_barang" value="<?php echo $barang->kode_barang; ?>">
        </div>

        <div class="form-group">
            <label>Nama Barang</label>
            <input class="form-control" name="kode_barang" value="<?php echo $barang->nama_barang; ?>">
        </div>

        <div class="form-group">
            <label>Nama Kategori</label>
            <select class="form-control" type="text" name="kode_kategori" id="kategori">
                <option><?php echo $barang->nama_kategori; ?></option>
                <?php  $kategori=$this->db->get('kategori'); ?>
                <?php foreach ($kategori->result() as $row) { ?>
                <option value="<?php echo $row->kode_kategori; ?>">
                               <?php echo $row->nama_kategori; ?>
                </option>
                <?php } ?>
                                                    
            </select>
        </div>

    </div>

    <div class="modal-footer">
        <button type='submit' name="submit" id="btnUpdate" class="btn btn-primary"> Edit</button>
        <button type="button" id="btnTutup" class="btn btn-default" data-dismiss="modal">Tutup</button>
    </div>
  </div>
</div>
</div>
<?php } ?>
                    <!-- modal message -->
                        <div id="modalMessage" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="msgHeader"></h4>
                                    </div>
                                    <div class="modal-body" id="msgMessage">
                                         
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="suksesAjak" class="btn btn-default" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- /modal message -->

                    <!-- Modal -->
                    <form name="registrasi" id="registrasi" method="post"  novalidate>
                        <div id="TambahModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                        <!-- Modal content-->
                        
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tambah Barang</h4>
                                    </div>
                                    <div class="modal-body">

                                            <div class="form-group">
                                                <label>SNID/Serial</label>
                                                <input class="form-control" type="text" name="snid" id="snid">
                                            </div>

                                          
                                            <div class='form-group'>
                                                <label>Kategori</label>
                                                    <select class='form-control' name="k_kategori" id='k_kategori'>
                                                        <option value='0'>Pilih Kategori</option>
                                                        <?php 
                                                            foreach ($kategori->result() as $kat) { ?>
                                                            <option value="<?php echo $kat->id_kategori; ?>">
                                                                <?php echo $kat->nama_kategori; ?>
                                                            </option>
                                                        <?php    }
                                                        ?>
                                                </select>
                                            </div>

                                            <div class='form-group'>
                                                <label>Merk</label>
                                                    <select class='form-control' name="merk" id='merk'>
                                                        <option value='0'>Pilih Merk</option>
                                                    </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Nama/Tipe Barang</label>
                                                <input class="form-control" type="text" name="nama_barang" id="nama_barang">
                                            </div>

                                        
                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
                                            </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END Modal -->

                    <!-- Modal Repair -->
                        <div id="RepairModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Hapus Kategori</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        Apa anda yakin akan menghapus Barang
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnRepair" class="btn btn-primary">Iya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Modal Repair -->
<script>
$(function(){

$.ajaxSetup({
type:"POST",
url: "<?php echo base_url('index.php/Barang/ambil') ?>",
cache: false,
});

$("#k_kategori").change(function(){

    var value=$(this).val();
    if(value>0){
        $.ajax({
            data:{modul:'mrk',id:value},
            success: function(respond){
            $("#merk").html(respond);
            }
        })
    }
});

/*$("#merk").change(function(){
    var value=$(this).val();
    if(value>0){
        $.ajax({
            data:{modul:'nBarang',id:value},
            success: function(respond){
            $("#nama_barang").html(respond);
            }
        })
    }
})*/


})

    $(document).ready(function() {
    $('#example').DataTable();
    }); 

    function openModalHapus(id_barang)
    {
        $("#btnRepair").attr('onclick',"updateHapus('"+id_barang+"')");
        $("#RepairModal").modal('show');
    }

    function updateHapus(id_barang)
    {
        //alert(id_barang);
        $("#RepairModal").modal('hide');
        
        url = "<?php echo base_url(); ?>index.php/Barang/hapus";
        $.post(url, 
        {
            id_barang:id_barang
        }).done(function(data){
            var obj = JSON.parse(data);

            $("#msgHeader").html(obj.header);
            $("#msgMessage").html(obj.message);
            $("#modalMessage").modal('show');
            if(obj.header=="sukses") {
                $("#suksesAjak").attr('onclick',"redirekSukses()");

            }
        });
    }


    function openModalTambah()
    {
        $("#btnTambah").attr('onclick',"simpan_form()");
        $("#TambahModal").modal('show');
    }

    function simpan_form()
    {
        $("#TambahModal").modal('hide');
        //var v_kode_barang = $("#kode_barang").val();
        var v_snid = $("#snid").val();
        var v_kode_kategori   = $("#k_kategori").val();
        var v_merk = $("#merk").val();
        var v_nama_barang = $("#nama_barang").val();
        var v_keteranagn   = $("#keterangan").val();

        var valsnid     = document.forms["registrasi"]["snid"].value;
        var valkategori    = document.forms["registrasi"]["k_kategori"].value;
        var valbarang    = document.forms["registrasi"]["nama_barang"].value;
        var valmerk    = document.forms["registrasi"]["merk"].value;
        var valketerangan    = document.forms["registrasi"]["keterangan"].value;

        if(valsnid==null || valsnid=="")
        {
            alert("SNID harus diisi");
            return false;
        }
        else if(valkategori=="Pilih Kategori")
        {
            alert("Kategori harus diisi");
            return false;
        }
        else if(valmerk=="Pilih Merk")
        {
            alert("Merk harus diisi");
            return false;
        }
        else if(valbarang==null || valbarang=="")
        {
            alert("Nama barang harus diisi");
            return false;
        }
        else if(valketerangan==null || valketerangan=="")
        {
            alert("Keterangan harus diisi");
            return false;
        }


        url = "<?php echo base_url(); ?>index.php/Barang/save";
        $.post(url, 
        {
            //kode_barang:v_kode_barang,
            snid:v_snid,
            k_kategori:v_kode_kategori,
            merk:v_merk,
            nama_barang:v_nama_barang,
            keterangan:v_keteranagn
            
        }).done(function(data){
            var obj = JSON.parse(data);

            alert(obj.message);
            //$("#msgHeader").html(obj.header);
            //$("#msgMessage").html(obj.message);
            //$("#modalMessage").modal('show');
            
            if(obj.header=="Sukses") {
                //swal("Good job!", "You clicked the button!", "success");
                //location.reload();
                  //$("#suksesAjak").attr('onclick',"redirekSukses()");
            }
        });
    }
  

    function redirekSukses()
    {
        location.reload();
    }




        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
</script>