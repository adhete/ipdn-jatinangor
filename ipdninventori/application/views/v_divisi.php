<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $judul ?></li>
            </ol>
        </div><!--/.row-->
        
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $subjudul ?></div>
                    <div class="panel-body">

                    

                   

                    <?php
                        $info=$this->session->flashdata('info');
                        if(!empty($info))
                        {
                            echo $info;
                        }
                    ?>
                   
                    <!-- Trigger the modal with a button -->
                    <br>
                     <a class="btn btn-primary" data-toggle="modal" onclick="openModalTambah()">
                        <div class="glyphicon glyphicon-plus"></div> Tambah Divisi/Lokasi
                    </a>

                    
                <table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th data-sortable="true">No</th>
                <th data-sortable="true">Kode Divisi</th>
                <th data-sortable="true">Nama Divisi</th>
               
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <?php $i=1 ?>
        <tbody>
        <?php foreach ($divisi as $row) { ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row->kode_bagian; ?></td>
                <td><?php echo $row->nama_bagian; ?></td>
                
                <td>
                   
                    <a class="btn btn-primary" data-toggle="modal" data-target="#EditModal" href="<?php echo base_url(); ?>index.php/Divisi/update/<?php echo $row->kode_bagian; ?>">
                        <div class="glyphicon glyphicon-edit"></div> Ubah</a>
                    <a class="btn btn-danger" onclick="openModalHapus('<?php echo $row->kode_bagian; ?>')">
                        <div class="glyphicon glyphicon-trash"></div> Hapus</a>
                </td>
            </tr>
        <?php } ?>
              
        </tbody>
    </table>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        
        
                                
            </div><!--/.col-->
        </div><!--/.row-->
    </div>  <!--/.main-->

    <!-- Trigger the modal with a button -->
                    
                    

                    <!-- EditModal -->
                        <div id="EditModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                </div>
                            </div>
                        </div>
                
                    <!-- END EditModal -->

                     <form name="my-form" id="my-form" method="post" >
                        <div id="TambahModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                        <!-- Modal content-->
                        
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tambah Bagian</h4>
                                    </div>
                                    <div class="modal-body">

                                            <div class="form-group">
                                                <label>Kode Bagian</label>
                                                <input class="form-control" type="text" name="kode_bagian" id="kode_bagian">
                                            </div>

                                            <div class="form-group">
                                                <label>Nama Bagian</label>
                                                <input class="form-control" type="text" name="nama_bagian" id="nama_bagian">
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END Modal -->

                    <!-- Modal hapus -->
                        <div id="RepairModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Hapus Divisi/Lokasi</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        Apa anda yakin akan menghapus Divisi/Lokasi
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnRepair" class="btn btn-primary">Iya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Modal hapus -->

                     <!-- modal message -->
                        <div id="modalMessage" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="msgHeader"></h4>
                                    </div>
                                    <div class="modal-body" id="msgMessage">
                                         
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="suksesAjak" class="btn btn-default" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- /modal message -->

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    });

    function openModalTambah()
    {

        $("#btnTambah").attr('onclick',"simpan_form()");
        
        $("#TambahModal").modal('show');

    }

    function simpan_form()
    {

        $("#TambahModal").modal('hide');
        //var v_kode_barang = $("#kode_barang").val();
        var v_kode_bagian = $("#kode_bagian").val();
        var v_nama_bagian = $("#nama_bagian").val();

        url = "<?php echo base_url(); ?>index.php/Divisi/save";
        $.post(url, 
        {
            //kode_barang:v_kode_barang,
            kode_bagian:v_kode_bagian,
            nama_bagian:v_nama_bagian,
            
        }).done(function(data){
            var obj = JSON.parse(data);

            alert(obj.message);
            //$("#msgHeader").html(obj.header);
            //$("#msgMessage").html(obj.message);
            ///$("#modalMessage").modal('show');
            
            if(obj.header=="Sukses") {
                //swal("Good job!", "You clicked the button!", "success");
                //location.reload();
                $("#suksesAjak").attr('onclick',"redirekSukses()");
            }
        });
    }

    function openModalHapus(kode_bagian)
    {
        $("#btnRepair").attr('onclick',"updateHapus('"+kode_bagian+"')");
        $("#RepairModal").modal('show');
    }

    function updateHapus(kode_bagian)
    {
        //alert(id_barang);
        $("#RepairModal").modal('hide');
        
        url = "<?php echo base_url(); ?>index.php/Divisi/hapus";
        $.post(url, 
        {
            kode_bagian:kode_bagian
        }).done(function(data){
            var obj = JSON.parse(data);

            $("#msgHeader").html(obj.header);
            $("#msgMessage").html(obj.message);
            $("#modalMessage").modal('show');
            if(obj.header=="sukses") {
                $("#suksesAjak").attr('onclick',"redirekSukses()");

            }
        });
    }

    function redirekSukses()
    {
        location.reload();
    }




        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
</script>