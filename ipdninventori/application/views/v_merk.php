<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $judul ?></li>
            </ol>
        </div><!--/.row-->
        
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Merk</div>
                    <div class="panel-body">

                    

                    <script type="text/javascript">
                        function validasi_input(form){
                            if (form.nama_merk.value == "")
                            {
                                alert("Nama Merk Tidak Boleh kosong!");
                                form.nama_merk.focus();
                            return (false);
                            }

                            if (form.id_kategori.value == "Pilih Merk"){
                                alert("Id Kategori Tidak Boleh kosong!");
                                form.id_kategori.focus();
                            return (false);
                            }
                            
                        }
                    </script>

                    <?php
                        $info=$this->session->flashdata('info');
                        if(!empty($info))
                        {
                            echo $info;
                        }
                    ?>
                   
        <!--LOKASI-->
        <form id="kirim" method="post">
			<a class="btn btn-primary" data-toggle="modal" onclick="openModalTambah()">
                        <div class="glyphicon glyphicon-plus"></div> Tambah Merk
                    </a>


        <!--END LOKASI-->
                    
                <table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th data-sortable="true">No</th>
                <th data-sortable="true">Nama Merk</th>
                <th data-sortable="true">Kategori</th>
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <?php $i=1 ?>
        <tbody>
        <?php foreach ($merk as $lihat) { ?>
            <tr>
                <td><?php echo $i++; ?> </td>
                <td><?php echo $lihat->nama_merk; ?></td>
                <td><?php echo $lihat->nama_kategori; ?></td>
                <td>
                   
                    <a class="btn btn-primary"data-toggle="modal" data-target="#EditModal" href="<?php echo base_url(); ?>index.php/Merk/update/<?php echo $lihat->id_merk; ?>">
                        <div class="glyphicon glyphicon-edit"></div> Ubah</a>

                    <a class="btn btn-danger" onclick="openModalHapus('<?php echo $lihat->id_merk; ?>')">
                        <div class="glyphicon glyphicon-trash"></div> Hapus</a>
                </td>
            </tr>
        <?php } ?>
              
        </tbody>
    </table>
    </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        
        
                                
            </div><!--/.col-->
        </div><!--/.row-->
    </div>  <!--/.main-->

    <!-- Trigger the modal with a button -->
   <!-- EditModal -->
                        <div id="EditModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                </div>
                            </div>
                        </div>
                
                    <!-- END EditModal -->
                    
                    

                    <form name="my-form" id="my-form" method="post">
                        <div id="TambahModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                        <!-- Modal content-->
                        
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tambah Merk</h4>
                                    </div>
                                    <div class="modal-body">

                                            <div class="form-group">
                                                <label>Nama Merk</label>
                                                <input class="form-control" type="text" name="nama_merk" id="nama_merk">
                                            </div>

                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <select class="form-control" type="text" name="id_kategori" id="id_kategori">
                                                            <option>Pilih Merk</option>
                                                    <?php $kategori=$this->db->get('kategori');
                                                          foreach ($kategori->result() as $row) { ?>
                                                            <option value="<?php echo $row->id_kategori;?>">
                                                                <?php echo $row->nama_kategori;?>
                                                            </option>
                                                   <?php  } ?> 
                                                </select>
                                                
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END Modal -->


                    <form name="my-form" id="my-form" method="post">
                        <div id="UpdateModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                        <!-- Modal content-->
                        <?php //foreach ($ as $lihat) { ?>
                            
                       
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tambah Bagian/Lokasi</h4>
                                    </div>
                                    <div class="modal-body">

                                            <div class="form-group">
                                                <label>Kode Lokasi/Bagian</label>
                                                <input class="form-control" type="text" name="kode_bagian" id="kode_bagian" value="<?php //echo kode_bagian; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Nama Lokasi/bagian</label>
                                                <input class="form-control" type="text" name="nama_bagian" id="nama_bagian" value="<?php// echo $lihat->nama_bagian; ?>">
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnTambah" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                                 <?php //} ?>
                            </div>
                        </div>
                    </form>

                     <!-- Modal Repair -->
                        <div id="RepairModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Hapus Kategori</h4>
                                    </div>
                                    <div class="modal-body">
                                        
                                        Apa anda yakin akan menghapus Merk
                                    </div>
                                    <div class="modal-footer">
                                        <button type='submit' name="submit" id="btnRepair" class="btn btn-primary">Iya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- END Modal Repair -->

                    <!-- modal message -->
                        <div id="modalMessage" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="msgHeader"></h4>
                                    </div>
                                    <div class="modal-body" id="msgMessage">
                                         
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="suksesAjak" class="btn btn-default" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- /modal message -->

                   


<script>
    $(document).ready(function() {
    $('#example').DataTable();
    });

    function openModalTambah()
    {
        $("#btnTambah").attr('onclick',"simpan_form()");
        $("#TambahModal").modal('show');
    }

    function simpan_form()
    {
        $("#TambahModal").modal('hide');
        //var v_kode_barang = $("#kode_barang").val();
        var v_nama_merk = $("#nama_merk ").val();
        var v_id_kategori = $("#id_kategori").val();

        url = "<?php echo base_url(); ?>index.php/Merk/save";
        $.post(url, 
        {
            //kode_barang:v_kode_barang,
            nama_merk:v_nama_merk,
            id_kategori:v_id_kategori,
            
        }).done(function(data){
            var obj = JSON.parse(data);

            alert(obj.message);
            //$("#msgHeader").html(obj.header);
            //$("#msgMessage").html(obj.message);
            //$("#modalMessage").modal('show');
            
            if(obj.header=="Sukses") {
                //swal("Good job!", "You clicked the button!", "success");
                //location.reload();
                  //$("#suksesAjak").attr('onclick',"redirekSukses()");
            }
        });
    }

    function openModalHapus(id_merk)
    {
        $("#btnRepair").attr('onclick',"updateHapus('"+id_merk+"')");
        $("#RepairModal").modal('show');
    }

    function updateHapus(id_merk)
    {
        //alert(id_barang);
        $("#RepairModal").modal('hide');
        
        url = "<?php echo base_url(); ?>index.php/Merk/hapus";
        $.post(url, 
        {
            id_merk:id_merk
        }).done(function(data){
            var obj = JSON.parse(data);

            $("#msgHeader").html(obj.header);
            $("#msgMessage").html(obj.message);
            $("#modalMessage").modal('show');
            if(obj.header=="sukses") {
                $("#suksesAjak").attr('onclick',"redirekSukses()");

            }
        });
    }

     function redirekSukses()
    {
        location.reload();
    }




        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
</script>