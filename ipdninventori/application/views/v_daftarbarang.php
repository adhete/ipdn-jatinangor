<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active"><?php echo $judul ?></li>
			</ol>
		</div><!--/.row-->
		
		
		<div class="row" style="margin-top: 15px;">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Daftar Barang</div>
					<div class="panel-body">

					<!-- Trigger the modal with a button -->
					
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
						<span class="glyphicon glyphicon-plus"></span> Tambah Barang
					</button>

					<!-- Modal -->
						<div id="myModal" class="modal fade" role="dialog">
  							<div class="modal-dialog">

    					<!-- Modal content-->
    							<div class="modal-content">
      								<div class="modal-header">
        								<button type="button" class="close" data-dismiss="modal">&times;</button>
        								<h4 class="modal-title">Tambah Barang</h4>
      								</div>
      								<div class="modal-body">
        								<p>
        									<div class="form-group">
        										<label>Nama Barang</label>
        										<input class="form-control" type="text" name="namabarang">
        									</div>
        								</p>
      								</div>
      								<div class="modal-footer">
        								<button type="button" class="btn btn-primary" data-dismiss="modal">Tambah</button>
        								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      								</div>
    							</div>

  							</div>
						</div>
					<!-- END Modal -->
					
				<table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th data-sortable="true">Nama Barang</th>
                <th data-sortable="true">Kode</th>
                <th data-sortable="true">Jenis</th>
                <th data-sortable="true">Jumah</th>
                <th data-sortable="true">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($daftarbarang as $lihat) { ?>
        	<tr>
                <td><?php echo $lihat->namabarang; ?></td>
                <td><?php echo $lihat->kode_barang; ?></td>
                <td><?php echo $lihat->jenis_barang; ?></td>
                <td><?php echo $lihat->jumlah; ?></td>
                <td>
                	<a href="#">Edit </a>| 
                	<a href="#">Hapus </a>
                </td>
            </tr>
        <?php } ?>
              
        </tbody>
    </table>
					</div>
				</div>
			</div>
		</div><!--/.row-->
		
		
								
			</div><!--/.col-->
		</div><!--/.row-->
	</div>	<!--/.main-->

<script>
	$(document).ready(function() {
    $('#example').DataTable();
    });




		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
</script>