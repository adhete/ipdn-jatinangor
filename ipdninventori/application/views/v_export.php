<?php

	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=Laporan-Data-Usaha-Bandung-.xls");

?>
<h1>Laporan Data Usaha Bandung</h1>
<br>
<br>
<table border="1">
	<tr>
		<td>#</td>
		<td>Kode Barang</td>
		<td>Nama Barang</td>
		<td>Kode Kategori</td>
		<td>Kode Bagian</td>
		<td>Keterangan</td>
	</tr>

	<?php if(is_array($row)) { $i=1; foreach ($row as $key => $value) { ?>
	<tr>
		<td><?=$i?></td>
		<td><?=htmlspecialchars($value['kode_barang'])?></td>
		<td><?=htmlspecialchars($value['nama_barang'])?></td>
		<td><?=htmlspecialchars($value['kode_kategori'])?></td>
		<td><?=htmlspecialchars($value['kode_bagian'])?></td>
		<td><?=htmlspecialchars($value['keterangan'])?></td>
	</tr>

	<?php $i++; }} ?>

</table>