

<?php echo form_open('Merk/proses_update'); ?>

	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Merk</h4>
    </div>

	<div class="modal-body">
		<input type="hidden" name="id_merk" value="<?php echo $merk->id_merk; ?>">
		<div class="form-group">
			<label>Nama Merk</label>
			<input class="form-control" name="nama_merk" value="<?php echo $merk->nama_merk; ?>">
		</div>

		<div class='form-group'>
            <label>Kategori</label>
                <select class='form-control' name="k_kategori" id='k_kategori'>
                    <option value="<?php echo $merk->id_kategori; ?>">
                    			   <?php echo $merk->nama_kategori; ?>
                    </option>
                <?php 
                $kategori=$this->db->get('kategori');
                foreach ($kategori->result() as $kat) { ?>
                    <option value="<?php echo $kat->id_kategori; ?>">
                                   <?php echo $kat->nama_kategori; ?>
                    </option>
                    <?php    }
                ?>
                </select>
        </div>
	</div>

	<div class="modal-footer">
		<button class="btn btn-primary" type="submit" name="submit">Ubah</button>
		<button onclick="refresh()" type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	</div>

<?php echo form_close();?>

<script type="text/javascript">
	function refresh(){
		location.reload();
	}

</script>