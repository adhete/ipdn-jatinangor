<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active"><?php echo $judul ?></li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $judul ?></h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
		<a href="<?php echo base_url();?>index.php/Barang_masuk">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked desktop computer and mobile"><use xlink:href="#stroked-desktop-computer-and-mobile"/></svg>
						</div>											
						<div class="col-sm-9 col-lg-7 widget-right2">
							<div class="large"><?php echo $total->num_rows(); ?></div>
							<div>Semua Barang</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Status/tersedia">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-green panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked monitor"><use xlink:href="#stroked-monitor"/></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right3">
							<div class="large"><?php echo $tersedia->num_rows(); ?></div>
							<div>Barang Tersedia</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Status/terpakai">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right4">
							<div class="large"><?php echo $terpakai->num_rows(); ?></div>
							<div>Barang terpakai</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Status/rusak">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"/></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right5">
							<div class="large"><?php echo $rusak->num_rows(); ?></div>
							<div>Barang Rusak</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Status/perbaiki">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-yellow panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked gear"><use xlink:href="#stroked-gear"/></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right6">
							<div class="large"><?php echo $diperbaiki->num_rows(); ?></div>
							<div>Diperbaiki</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Status/rusak_total">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-dark panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"/></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right7">
							<div class="large"><?php echo $rusaktotal->num_rows(); ?></div>
							<div>Rusak Total</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		<a href="<?php echo base_url();?>index.php/Divisi">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"/></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right2">
							<div class="large"><?php echo $lokasi->num_rows(); ?></div>
							<div>Divisi/Bagian</div>
						</div>
					</div>
				</div>
			</div>
		</a>

		</div><!--/.row-->
			
								
			</div><!--/.col-->
		</div><!--/.row-->
	</div>	<!--/.main-->

<script>
	$(document).ready(function() {
    $('#example').DataTable();
    });




		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
</script>