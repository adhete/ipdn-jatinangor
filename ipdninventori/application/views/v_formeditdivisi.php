<?php echo form_open('Divisi/proses_update'); ?>

	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Kategori</h4>
    </div>

	<div class="modal-body">
		<input type="hidden" name="id_bagian" value="<?php echo $bagian->id_bagian; ?>">
		<div class="form-group">
			<label>Kode Kategori</label>
			<input class="form-control" name="kode_bagian" value="<?php echo $bagian->kode_bagian; ?>">
		</div>

		<div class="form-group">
			<label>Nama Kategori</label>
			<input class="form-control" name="nama_bagian" value="<?php echo $bagian->nama_bagian; ?>">
		</div>
	</div>

	<div class="modal-footer">
		<button class="btn btn-primary" type="submit" name="submit">Ubah</button>
		<button onclick="refresh()" type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	</div>

<?php echo form_close();?>

<script type="text/javascript">
	function refresh(){
		location.reload();
	}
</script>