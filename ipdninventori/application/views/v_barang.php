<script type="text/javascript">
    var jml_data = 0;
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $judul ?></li>
            </ol>
        </div><!--/.row-->
        
        
        <div class="row" style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Daftar Barang</div>
                    <div class="panel-body">

                    

                   

                    <?php
                        $info=$this->session->flashdata('info');
                        if(!empty($info))
                        {
                            echo $info;
                        }
                    ?>
                   
        <!--LOKASI-->
        <form id="kirim" method="post" onsubmit="return false">
            <div class="input-group">
            <label class="control-label" for="inputEmail">Pilih Divisi/Lokasi</label>
            
            <select  class="form-control" style="width: 200px;" type="text" name="kode_bagian" id="kode_bagian">
                <option>Pilih Divisi/Lokasi</option>
                <?php foreach ($bagian as $row) { ?>
                <option value="<?php echo $row->kode_bagian; ?>">
                    <?php echo $row->nama_bagian; ?>
                </option>
                <?php } ?>
            </select>
            

            <div class="input-group-btn"> 
               
                    <button style="margin-right:1000px; margin-top:25px" onclick="simpanlokasi()"  class="btn btn-primary" name="simpan" id="simpan" data-placement="right">
                        Ubah</button>
               
            </div>
            </div>
        
        <!--END LOKASI-->
                    <span id="jml"></span>
                <table data-toggle="table" data-show-refresh="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
        <thead>
            <tr>
                <th class="empty"></th>
                <th data-sortable="true">Kode Barang</th>
                <th data-sortable="true">Nama Barang</th>
                <th data-sortable="true">Kategori</th>
            </tr>
        </thead>
        <?php $i=1 ?>
        <tbody>
        <?php foreach ($daftarbarang as $lihat) { ?>
            <tr>
                <td width="30" class="empty">
                    <input onchange="addminjml('#ck_<?=$i?>')" id="ck_<?=$i?>" name="idbarang[]" type="checkbox" value="<?php echo $lihat->id_barang; ?>" >
                </td>
                <td><?php echo $lihat->kode_kategori; 
                          echo"/"; 
                          echo strtoupper(substr($lihat->nama_merk,0,3));
                          echo"/"; 
                          echo $lihat->snid;
                    ?>
                <td><?php echo $lihat->nama_barang; ?></td>
                <td><?php echo $lihat->nama_kategori; ?></td>
            </tr>
        <?php $i++;} ?>
              
        </tbody>
    </table>
    </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
        
        
                                
            </div><!--/.col-->
        </div><!--/.row-->
    </div>  <!--/.main-->

    <!-- Trigger the modal with a button -->
                    
                    

                    <!-- EditModal -->
                        <div id="EditModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                </div>
                            </div>
                        </div>
                
                    <!-- END EditModal -->

                    <script type="text/javascript">
                    function simpanlokasi()
                    {
                        var string = $("#kirim").serialize();
                        console.log(string+'&jml_data='+jml_data);
                        string += '&jml_data='+jml_data;
                        $.ajax({
                            type    : 'POST',
                            url     : '<?php echo site_url();?>/BarangBaru/simpanlokasi',
                            data    : string,
                            success : function(data){
                                if(jml_data<=0){
                                    alert("Pilih data terlebih dahulu");
                                } else {
                                    alert("Data Berhasil Ditambahkan");
                                    location.reload();
                                }
                            }
                        });
                    }

                    function addminjml(idInput)
                    {
                        var isck = $(idInput+':checkbox:checked').length > 0;
                        if(isck) {
                            jml_data++;
                        } else {
                            if(jml_data>0) {
                                jml_data--;
                            }
                        }
                        //console.log('jml='+jml_data);
                    }
                    </script>


<script>
    $(document).ready(function() {
    $('#example').DataTable();
    });




        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){        
                $(this).find('em:first').toggleClass("glyphicon-minus");      
            }); 
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
          if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
          if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
</script>