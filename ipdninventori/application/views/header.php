<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo base_url()."assets/"; ?>/img/ipdn.png" type="image/gif">
<title>Inventori Barang IT</title>

<link href="<?php echo base_url()."assets/"; ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()."assets/"; ?>css/datepicker3.css" rel="stylesheet">
<link href="<?php echo base_url()."assets/"; ?>css/styles.css" rel="stylesheet">
<link href="<?php echo base_url()."assets/"; ?>css/sweetalert.css" rel="stylesheet">
<!--sweet alert-->
<link href="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">


<!--Icons-->
<script src="<?php echo base_url()."assets/"; ?>js/sweetalert.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>js/lumino.glyphs.js"></script>
<script src="<?php echo base_url()."assets/"; ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>js/alert.js"></script>
<link href="<?php echo base_url()."assets/"; ?>css/bootstrap-table.css" rel="stylesheet">

<script src="<?php echo base_url()."assets/"; ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>js/bootstrap-table.js"></script>

<script src="<?php echo base_url()."assets/"; ?>js/parsley.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>js/jquery.validate.min"></script>
jquery.validate.min

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="<?php echo base_url()."assets/"; ?>bootstrap-sweetalert/jquery.sweet-alert.init.js"></script>


<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="<?php echo base_url(); ?>"><img style="width:50px; height:50px;" src="<?php echo base_url()."assets/img/"; ?>ipdn.png"/><span> Inventory Barang IT</span></a>
				<ul style="margin-top: 2%" class="user-menu">
					
						<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg><?php echo anchor('Login/logout','Keluar');?>
					
				</ul>
				
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>


	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	

