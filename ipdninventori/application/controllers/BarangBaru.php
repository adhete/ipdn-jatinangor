<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarangBaru extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		$this->load->helper('url');
      	$this->load->model('MasterMdl');
      	chek_session();
    }

	public function index()
	{
		/*$this->load->model('M_mymodel');
		$isi['judul']	='Daftar Barang';
		$isi['subjudul']='';
		/*$isi['data']	=$this->db->get('barang')->result();
		  
		$isi['data']	=$this->M_mymodel->GetBarang()->result();*/
		$data['judul']	='Data Barang';
		$data['daftarbarang']=$this->MasterMdl->GetBarangGudang()->result();
		$data['bagian']=$this->MasterMdl->get_divisi();



		
		

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		//return $this->load->view("sidebar",$data,true);
		return $this->load->view("sidebar/sidebarnewbarang",$data,true);
	}



	public function update($kode_kategori)
    {
        $data['kategori'] = $this->M_kategori->get_detail_barang($kode_kategori)->row();
        $this->load->view('v_formeditkategori',$data);
    }
 
    public function proses_update()
    {
        $kode_kategori['kode_kategori'] = $this->input->post('kode_kategori');
        $data = array('kode_kategori' => $this->input->post('kode_kategori') ,
                    'nama_kategori' => $this->input->post('nama_kategori'),
                    
                    );
        $this->M_kategori->update_barang($data,$kode_kategori);
        $this->session->set_flashdata('info','Data Berhasil di Update');
        redirect('C_kategori','refresh');
    }
 
    public function delete($kode_kategori)
    {
        $this->M_kategori->delete_barang($kode_kategori);
        redirect('C_kategori','refresh');
    }



    public function simpanlokasi()
	  {      
	  	$kode = $this->input->post('kode_bagian');
	  	$key = array();
	  	$data=array(
			      'kode_bagian'=> $kode,
			    );
	  	foreach ($_POST['idbarang'] as $key => $value) {
	  		//dump($value);
	  		//$key['id_barang'] = $value; 
	  		$key = array(
	  			'id_barang' => $value, 
	  		);
	  		//$this->input->post('idbarang');
			$this->MasterMdl->update_bagian($data, $key);
	  	}
	    //redirect('Barang','refresh');
	  }




	
}