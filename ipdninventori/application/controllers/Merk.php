<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		    $this->load->helper('url');
      	$this->load->model('MasterMdl');
        chek_session();
    }

	public function index()
	{
		$data['judul']	='Data Merk';
		$data['merk']=$this->MasterMdl->get_merk()->result();
    

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_merk",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar/sidebarlokasi",$data,true);
	}

	public function update($id_merk)
    {
        $data['merk'] = $this->MasterMdl->get_detail_merk($id_merk)->row();
        $this->load->view('v_formeditmerk',$data);
    }
 
    public function proses_update()
    {
        $id_merk['id_merk'] = $this->input->post('id_merk');
        $data = array(
                    'id_merk' => $this->input->post('id_merk'),
                    'nama_merk' => $this->input->post('nama_merk') ,
                    'id_kategori' => $this->input->post('k_kategori'),
                    
                    );
        $this->MasterMdl->ubah_merk($data,$id_merk);
        $this->session->set_flashdata('info','Data Berhasil di Update');
        redirect('Merk','refresh');
    }
 


  ////////////////////////////////////////////////////////////////
  public function save()
  {
    $data=array(
                      //'kode_barang'=> $this->input->post('kode_barang'),
                      'nama_merk'=> $this->input->post('nama_merk'),
                      'id_kategori'=> $this->input->post('id_kategori'),
                      
                    );

    $update = $this->db->insert('merk', $data);
    //$update = $this->MasterMdl->tambah_merk($data);
    //$save = $this->db->insert('barang', $kode_barang, $nama_barang, $kode_kategori, $keterangan);
    if($update) {
        $header  = "Sukses";
        $message = "Merk Berhasil Di Tambahkan";
      } else {
        $header  = "Error";
        $message = "Merk Gagal Di Tambahkan";
      }
      $msg = array(
        'header' => $header,
        'message'=> $message 
      );
      echo json_encode($msg);
  }


   public function hapus()
    {
        $id_brg = $_POST['id_merk'];
        //echo "Masuk Kontroler, kode=".$kode;
        //$data['id_barang'] = $this->M_barang_rusak->get_detail_barang($id_barang)->row();
        //$this->load->view('formedit/v_editrepair',$data);
        $update = $this->MasterMdl->hapus_merk($id_brg);
        if($update) {
            $header  = "error";
            $message = "Merk Gagal Di Hapus";
            
        } else {
            $header  = "sukses";
            $message = "Merk Berhasil Di Hapus";
        }
        $msg = array(
            'header' => $header,
            'message'=> $message 
        );
        echo json_encode($msg);
    }


  

	
}