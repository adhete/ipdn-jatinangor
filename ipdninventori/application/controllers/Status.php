<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		$this->load->helper('url');
      	$this->load->model('StatusMdl');
      	$this->load->model('MasterMdl');
      	chek_session();
    }

	public function tersedia()
	{
		$data['judul']	='Barang Tersedia';
		$data['subjudul']	='Data Barang Tersedia';
		$data['bagian'] = $this->MasterMdl->get_divisi();
		$data['daftarbarang']=$this->StatusMdl->GetBarangTersedia()->result();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang_tersedia",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function terpakai()
	{
		$data['judul']	='Barang Terpakai';
		$data['subjudul']	='Data Barang Terpakai';
		$data['bagian'] = $this->MasterMdl->get_divisi();
		$data['daftarbarang']=$this->StatusMdl->GetBarangTerpakai()->result();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang_terpakai",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function rusak()
	{
		$data['judul']	='Barang Rusak';
		$data['subjudul']	='Data Barang Rusak';
        $data['bagian'] = $this->MasterMdl->get_divisi();
		$data['daftarbarang']=$this->StatusMdl->GetBarangRusak()->result();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang_rusak",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function perbaiki()
	{
		$data['judul']	='Barang Diperpaiki';
		$data['subjudul']	='Data Barang Diperpaiki';
        $data['bagian'] = $this->MasterMdl->get_divisi();
		$data['daftarbarang']=$this->StatusMdl->GetBarangPerbaiki()->result();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang_diperbaiki",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function rusak_total()
	{
		$data['judul']	='Barang Rusak Total';
		$data['subjudul']	='Data Barang Rusak Total';
		$data['bagian'] = $this->MasterMdl->get_divisi();
		$data['daftarbarang']=$this->StatusMdl->GetBarangRusaktotal()->result();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barang_rusaktotal",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		//return $this->load->view("sidebar",$data,true);
		return $this->load->view("sidebar",$data,true);
	}

	public function updateStatusTerpakai()
    {
    	$id_brg = $_POST['id_barang'];
    	$update = $this->StatusMdl->update_terpakai($id_brg);
    	if($update) {
    		$header  = "Sukses";
    		$message = "Status Berhasil Di Ubah";
    	} else {
    		$header  = "Error";
    		$message = "Status Gagal Di Ubah";
    	}
    	$msg = array(
    		'header' => $header,
    		'message'=> $message 
    	);
    	echo json_encode($msg);
    }

    public function updateStatusTersedia()
    {
    	$id_brg = $_POST['id_barang'];
    	$update = $this->StatusMdl->update_tersedia($id_brg);
    	if($update) {
    		$header  = "Sukses";
    		$message = "Status Berhasil Di Ubah";
    	} else {
    		$header  = "Error";
    		$message = "Status Gagal Di Ubah";
    	}
    	$msg = array(
    		'header' => $header,
    		'message'=> $message 
    	);
    	echo json_encode($msg);
    }

     public function updateStatusRusak()
    {
    	$id_brg = $_POST['id_barang'];
    	$update = $this->StatusMdl->update_rusak($id_brg);
    	if($update) {
    		$header  = "Sukses";
    		$message = "Status Berhasil Di Ubah";
    	} else {
    		$header  = "Error";
    		$message = "Status Gagal Di Ubah";
    	}
    	$msg = array(
    		'header' => $header,
    		'message'=> $message 
    	);
    	echo json_encode($msg);
    }

    public function updateRusakTotal()
    {
    	$id_brg = $_POST['id_barang'];
    	$update = $this->StatusMdl->update_rusak_total($id_brg);
    	if($update) {
    		$header  = "Sukses";
    		$message = "Status Berhasil Di Ubah";
    	} else {
    		$header  = "Error";
    		$message = "Status Gagal Di Ubah";
    	}
    	$msg = array(
    		'header' => $header,
    		'message'=> $message 
    	);
    	echo json_encode($msg);
    }

    public function updateStatusPerbaiki()
    {
    	$id_brg = $_POST['id_barang'];
    	//echo "Masuk Kontroler, kode=".$kode;
        //$data['id_barang'] = $this->M_barang_rusak->get_detail_barang($id_barang)->row();
        //$this->load->view('formedit/v_editrepair',$data);
    	$update = $this->StatusMdl->update_perbaiki($id_brg);
    	if($update) {
    		$header  = "sukses";
    		$message = "Status Berhasil Di Ubah";
    	} else {
    		$header  = "error";
    		$message = "Status Gagal Di Ubah";
    	}
    	$msg = array(
    		'header' => $header,
    		'message'=> $message 
    	);
    	echo json_encode($msg);
    }


    public function simpanlokasi()
	  {      
	  	$kode = $this->input->post('kode_bagian');
	  	$key = array();
	  	$data=array(
			      'kode_bagian'=> $kode,
			    );
	  	foreach ($_POST['idbarang'] as $key => $value) {
	  		//dump($value);
	  		//$key['id_barang'] = $value; 
	  		$key = array(
	  			'id_barang' => $value, 
	  		);
	  		//$this->input->post('idbarang');
			$this->MasterMdl->update_bagian($data, $key);
	  	}
	    //redirect('Barang','refresh');
	  }

	

	

	
}