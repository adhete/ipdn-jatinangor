<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detailbarang extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		$this->load->helper('url');
      	$this->load->model('M_detailbarang');
      	chek_session();
    }

	public function index()
	{
		$data['judul']	='Detail Barang';

		$data['detailbarang'] = $this->M_detailbarang->GetDetailBarang($kode_barang)->row();

		
		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_detailbarang",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar",$data,true);
	}


	public function update($kode_barang)
    {
       
        $data['detailbarang'] = $this->M_detailbarang->get_detail_barang($kode_barang)->row();
        $this->load->view('barang',$data);
    }


}