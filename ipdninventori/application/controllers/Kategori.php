<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		$this->load->helper('url');
      	$this->load->model('MasterMdl');
        chek_session();
    }

	public function index()
	{
		$data['judul']	='Daftar Kategori Barang';
		$data['daftarkategori']=$this->MasterMdl->get_kategori()->result();
        //$data['jumlah']=$this->MasterMdl->get_barangdua();

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_kategori",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar/sidebarkategori",$data,true);
	}


	/*-------------Fungsi CRUD---------------------*/
  


	public function update($kode_kategori)
    {
        $data['kategori'] = $this->MasterMdl->get_detail_kategori($kode_kategori)->row();
        $this->load->view('v_formeditkategori',$data);
    }
 
    public function proses_update()
    {
        $id_kategori['id_kategori'] = $this->input->post('id_kategori');
        $data = array(
                        'id_kategori' => $this->input->post('id_kategori'),
        				'kode_kategori' => $this->input->post('kode_kategori'),
                    	'nama_kategori' => $this->input->post('nama_kategori'),
                    );
        $this->MasterMdl->ubah_kategori($data,$id_kategori);
        $this->session->set_flashdata('info','Data Berhasil di Update');
        redirect('Kategori','refresh');
    }

    ////////////////////////////////////////////
    public function hapus()
    {
        $id_brg = $_POST['id_kategori'];
        //echo "Masuk Kontroler, kode=".$kode;
        //$data['id_barang'] = $this->M_barang_rusak->get_detail_barang($id_barang)->row();
        //$this->load->view('formedit/v_editrepair',$data);
        $update = $this->MasterMdl->hapus_kategori($id_brg);
        if($update) {
            $header  = "error";
            $message = "Kategori Gagal Di Hapus";
            
        } else {
            $header  = "sukses";
            $message = "Kategori Berhasil Di Hapus";
        }
        $msg = array(
            'header' => $header,
            'message'=> $message 
        );
        echo json_encode($msg);
    }

  public function save()
  {
    $data=array(
                      //'kode_barang'=> $this->input->post('kode_barang'),
                      'kode_kategori'=> $this->input->post('kode_kategori'),
                      'nama_kategori'=> $this->input->post('nama_kategori'),
                      
                    );

    $update = $this->db->insert('kategori', $data);
    //$update = $this->MasterMdl->tambah_kategori($data);
    //$save = $this->db->insert('barang', $kode_barang, $nama_barang, $kode_kategori, $keterangan);
    if($update) {
        $header  = "Sukses";
        $message = "Kategori Berhasil Di Tambahkan";
      } else {
        $header  = "Error";
        $message = "Kategori Gagal Di Tambahkan";
      }
      $msg = array(
        'header' => $header,
        'message'=> $message 
      );
      echo json_encode($msg);
  }

	
}