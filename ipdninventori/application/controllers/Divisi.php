<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		    $this->load->helper('url');
      	$this->load->model('MasterMdl');
        chek_session();
    }

	public function index()
	{
		$data['judul']	='Daftar Divisi';
		$data['subjudul']	='Daftar Divisi';
		$data['divisi']=$this->MasterMdl->get_divisi();

		
		

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_divisi",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar/sidebarbagian",$data,true);
	}


	/*-------------Fungsi CRUD---------------------*/

	public function update($kode_bagian)
    {
        $data['bagian'] = $this->MasterMdl->get_detail_bagian($kode_bagian)->row();
        $this->load->view('v_formeditdivisi',$data);
    }
 
    public function proses_update()
    {
        $id_bagian['id_bagian'] = $this->input->post('id_bagian');
        $data = array(
                        'id_bagian' => $this->input->post('id_bagian'),
        				        'kode_bagian' => $this->input->post('kode_bagian'),
                    	  'nama_bagian' => $this->input->post('nama_bagian'),
                    );
        $this->MasterMdl->ubah_divisi($data,$id_bagian);
        $this->session->set_flashdata('info','Data Berhasil di Update');
        redirect('Divisi','refresh');
    }
 
     public function hapus()
    {
        $id_brg = $_POST['kode_bagian'];
        //echo "Masuk Kontroler, kode=".$kode;
        //$data['id_barang'] = $this->M_barang_rusak->get_detail_barang($id_barang)->row();
        //$this->load->view('formedit/v_editrepair',$data);
        $update = $this->MasterMdl->hapus_divisi($id_brg);
        if($update) {
            $header  = "error";
            $message = "Kategori Gagal Di Hapus";
            
        } else {
            $header  = "sukses";
            $message = "Kategori Berhasil Di Hapus";
        }
        $msg = array(
            'header' => $header,
            'message'=> $message 
        );
        echo json_encode($msg);
    }


  public function save()
  {
    $data=array(
                      //'kode_barang'=> $this->input->post('kode_barang'),
                      'kode_bagian'=> $this->input->post('kode_bagian'),
                      'nama_bagian'=> $this->input->post('nama_bagian'),
                      
                    );

    $update = $this->db->insert('bagian', $data);
    //$update = $this->M_barangmasuk->tambahBrg($data);
    //$save = $this->db->insert('barang', $kode_barang, $nama_barang, $kode_kategori, $keterangan);
    if($update) {
        $header  = "Sukses";
        $message = "Divisi/Lokasi Berhasil Di Tambahkan";
      } else {
        $header  = "Error";
        $message = "Divisi/Lokasi Gagal Di Tambahkan";
      }
      $msg = array(
        'header' => $header,
        'message'=> $message 
      );
      echo json_encode($msg);
  }

	
}