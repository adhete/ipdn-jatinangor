<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class CetakLaporan extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->model('MasterMdl'); // memanggil model MasterMdl
        chek_session();
    }
    public function export(){
        $ambildata = $this->MasterMdl->export_laporan();
         
        if(count($ambildata)>0){
            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()
                  ->setCreator("UPTIK IPDN") //creator
                    ->setTitle("Laporan Inventaris Barang");  //file title
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Laporan Inventaris Barang'); //sheet title
             
            $objget->getStyle("A2:H2")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '92d050')
                    ),
                    'font' => array(
                        'color' => array('rgb' => '000000')
                    )
                )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
             
            $val = array("Kode Barang","SNID","Nama Barang","Kategori","Lokasi","tgl_pengadaan","Status","keterangan");
             
            for ($a=0;$a<8; $a++) 
            {
                $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25); // KODE BARANG
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18); // SNID
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // NAMA BARANG
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15); // KATEGORI
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // LOKASI
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15); // TGL PENGADAAN
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13); // STATUS
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25); // KETERANGAN
             
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 3;
            foreach ($ambildata as $frow){
                 
                //pemanggilan sesuaikan dengan nama kolom tabel

                $objset->setCellValue("A".$baris, $frow->kode_kategori."/".substr($frow->nama_merk,0, 3)."/".$frow->snid);
                $objset->setCellValue("B".$baris, $frow->snid);
                $objset->setCellValue("C".$baris, $frow->nama_barang); //membaca data nama
                $objset->setCellValue("D".$baris, $frow->nama_kategori);
                $objset->setCellValue("E".$baris, $frow->nama_bagian);
                $objset->setCellValue("F".$baris, $frow->tgl_pengadaan);
                $objset->setCellValue("G".$baris, $frow->status); //membaca data alamat
                $objset->setCellValue("H".$baris, $frow->keterangan); //membaca data kontak

                 
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
            }
             
            $objPHPExcel->getActiveSheet()->setTitle('Data Export');
 
            $objPHPExcel->setActiveSheetIndex(0);  
            $filename = urlencode("Data Inventori Barang IT ".date("Y-m-d H:i:s").".xls");
               
              header('Content-Type: application/vnd.ms-excel'); //mime type
              header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
              header('Cache-Control: max-age=0'); //no cache
 
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');                
            $objWriter->save('php://output');
        }else{
            redirect('Excel');
        }
    }
}
