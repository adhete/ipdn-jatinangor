<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	function __construct() {
        parent::__construct();
      	$this->load->helper('form');
		    $this->load->helper('url');
      	//$this->load->model('M_barangmasuk');
        chek_session();
        $this->load->model('MasterMdl','',TRUE);

    }

	public function data_barang()
	{
		$data['judul']	='Pengadaan Barang';
		$data['daftarbarang']=$this->MasterMdl->get_barang();
    $data['kategori']=$this->MasterMdl->ambil_kategori();


		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_barangmasuk",$data,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar/sidebarbarang",$data,true);
	}


  public function detail($id_barang)
  {
        $data['judul']  ='Detail Barang';
        $data['subjudul']  ='Detail Barang';
        $data['detail'] = $this->MasterMdl->get_detail($id_barang);
       
        $com = array(
        "header" => $this->html_header(),
        "content" =>  $this->load->view("v_detailbarang",$data,true),
        "sidebar" => $this->html_sidebar(),
      );
        //$this->load->view('v_detailbarang',$data);
        $this->load->view("index",$com);
  }

 


  public function save()
  {
    $data=array(
                      //'kode_barang'=> $this->input->post('kode_barang'),
                      'snid'=> $this->input->post('snid'),
                      'nama_barang'=> $this->input->post('nama_barang'),
                      'merk'=> $this->input->post('merk'),
                      'status'=> "Tersedia",
                      //'kode_barang'=> $this->input->post('snid, merk'),
                      'keterangan'=> $this->input->post('keterangan'),
                      'kode_kategori'=> $this->input->post('k_kategori'),
                      'kode_bagian' => "GDU"
                    );
                      $this->db->set('tgl_pengadaan', 'NOW()', FALSE); 

    $update = $this->db->insert('barang', $data);
    //$update = $this->M_barangmasuk->tambahBrg($data);
    //$save = $this->db->insert('barang', $kode_barang, $nama_barang, $kode_kategori, $keterangan);
    if($update) {
        $header  = "Sukses";
        $message = "Barang Berhasil Di Tambahkan";
      } else {
        $header  = "Error";
        $message = "Barang Gagal Di Tambahkan";
      }
      $msg = array(
        'header' => $header,
        'message'=> $message 
      );
      echo json_encode($msg);
  }

  
  public function ambil()
  {
    $modul=$this->input->post('modul');
    $id=$this->input->post('id');


    if($modul=="mrk")
    {
     echo $this->MasterMdl->merk($id);
    }

  }

  public function hapus()
    {
        $id_brg = $_POST['id_barang'];
        //echo "Masuk Kontroler, kode=".$kode;
        //$data['id_barang'] = $this->M_barang_rusak->get_detail_barang($id_barang)->row();
        //$this->load->view('formedit/v_editrepair',$data);
        $update = $this->MasterMdl->hapus_barang($id_brg);
        if($update) {
            $header  = "error";
            $message = "Barang Gagal Di Hapus";
            
        } else {
            $header  = "sukses";
            $message = "Barang Berhasil Di Hapus";
        }
        $msg = array(
            'header' => $header,
            'message'=> $message 
        );
        echo json_encode($msg);
    }

    public function ubah($id_barang)
    {
        $data['barang'] = $this->MasterMdl->getdetailbarang($id_barang)->row();
        //$data['kate']=$this->M_barangmasuk->ambil_kategori();
        $this->load->view('v_formeditbarang',$data);
    }


    public function proses_update()
    {
        $id['id_barang'] = $this->input->post('id_barang');
        //$id['snid']= $this->input->post('snid');
        $data = array(
          //$id_barang['id_barang'] = $this->input->post('id_barang'),
                      'id_barang' => $this->input->post('id_barang'),
                      'snid'=> $this->input->post('snid'),
                      'nama_barang'=> $this->input->post('nama_barang'),
                      'kode_kategori'=> $this->input->post('k_kategori'),
                      'merk'=> $this->input->post('merk'),
                      'keterangan'=> $this->input->post('keterangan'),
                      
                    );
        $this->MasterMdl->ubah_barang($data,$id);
        $this->session->set_flashdata('info','Data Berhasil di Update');
        redirect('Barang/data_barang','refresh');
    }






 

	
}