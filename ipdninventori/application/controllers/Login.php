<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('MasterMdl');
    }

	function FormLogin()
    {
        if(isset($_POST['submit'])){
            
            // proses login disini
            $username   =   $this->input->post('username');
            $password   =   $this->input->post('password');
            $hasil=  $this->MasterMdl->login($username,$password);
            if($hasil==1)
            {
                // update last login
                //$this->db->where('username',$username);
                //$this->db->update('t_admin',array('last_login'=>date('Y-m-d')));
                $this->session->set_userdata(array('status_login'=>'oke','username'=>$username));
                redirect('Dashboard');
                //redirect('login/FormLogin');
            }
            else{
               
                //redirect('dashboard');
                $this->session->set_flashdata('info','Username atau password salah, Ulangi!');
           
                redirect('Login/FormLogin');
            }
        }
        else{
            //$this->load->view('form_login2');
            //chek_session_login();
            //$this->template->load('template_login','form_login');
            chek_session_login();
            $this->load->view('v_login');
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('Login/FormLogin');
    }
	
}
?>
<script>
function alert(){
	window.alert('Selamat datang');
}
</script>