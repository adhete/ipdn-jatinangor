<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('StatusMdl');
        chek_session();
    }


	public function index()
	{
		
		$isi['judul']	='Halaman Utama';
		$isi['subjudul']='';
		$isi['total'] = $this->StatusMdl->GetTotal();
		$isi['tersedia'] = $this->StatusMdl->GetBarangTersedia();
		$isi['terpakai'] = $this->StatusMdl->GetBarangTerpakai();
		$isi['rusak'] = $this->StatusMdl->GetBarangRusak();
		$isi['diperbaiki'] = $this->StatusMdl->GetBarangPerbaiki();
		$isi['rusaktotal'] = $this->StatusMdl->GetBarangRusakTotal();
		$isi['lokasi'] = $this->StatusMdl->GetLokasi();
	
		

		$com = array(
				"header" => $this->html_header(),
				"content" =>  $this->load->view("v_home",$isi,true),
				"sidebar" => $this->html_sidebar(),
			);
		$this->load->view("index",$com);

	}

	public function html_header(){
		$data = array();
		return $this->load->view("header",$data,true);
	}

	public function html_sidebar(){
		$data = array();
		return $this->load->view("sidebar",$data,true);
	}



	

}
