<?php 
class MasterMdl extends CI_Model {
	var $tabel_kategori='kategori';
	var $tabel_merk='merk';

	function __construct() {
        parent::__construct();
      	$this->load->database();
    }

    function login($username,$password)
    {
        $chek=  $this->db->get_where('admin',array('username'=>$username,'password'=>  md5($password)));
        if($chek->num_rows()>0){
            return 1;
        }
        else{
            return 0;
        }
    }

	public function get_barang()
	{
		return $row = $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN merk ON barang.merk = merk.id_merk
								 ORDER BY id_barang DESC'
								);


	}

	public function get_detail_barang($kode_barang){
		//return $this->db->get_where('barang', array('kode_barang' => $kode_barang));
		$this->db->select ('*'); 
    	$this->db->from ('barang');
    	$this->db->join ('kategori', 'barang.kode_kategori = kategori.kode_kategori');
    	$this->db->where ('barang.kode_barang', $kode_barang);
   	 	$query = $this->db->get ();
   	 	return $query->result ();
	}

	public function ubah_barang($data,$id){
		$this->db->update("barang",$data,$id);
	}

	public function get_detail($id_barang){
   	 	return $row = $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.id_barang='.$id_barang.''
								);
	}

	function ambil_kategori()
	{
		$this->db->order_by('nama_kategori','ASC');
		$katekoris= $this->db->get('kategori');
		return $katekoris->result();
	}

	function merk($katId)
	{
		
		$merk="<option value='0'>Pilih Merk</pilih>";
		$this->db->order_by('nama_merk','ASC');
		$mrk= $this->db->get_where('merk',array('id_kategori'=>$katId));
			foreach ($mrk->result() as $data )
			{
				$merk.= "<option value='  $data->id_merk'>$data->nama_merk</option>";
				//$merk.= "<option value='$data->id_merk'>$data->nama_merk</option>";
				//$merk.= "<option value='$data[id_merk]'>$data[nama_merk]</option>";
			}
		return $merk;

	}

	public function hapus_barang($id_barang){
		$this->db->where('id_barang', $id_barang);
		$this->db->delete('barang'); 
	}
	
	public function getdetailbarang($id_barang)
	{
		//return $this->db->get_where('barang', array('id_barang' => $id_barang));
		return $row = $this->db->query('SELECT * FROM barang 
								 		JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 		JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 		JOIN merk ON barang.merk = merk.id_merk
								 		WHERE barang.id_barang='.$id_barang.''
								);
	}

	///---------------------------------------------END BARANG-----------------------------------------------------------//

	public function get_kategori()
	{
		return $this->db->get('kategori');
	}


	public function get_detail_kategori($kode_kategori)
	{
		return $this->db->get_where('kategori', array('kode_kategori' => $kode_kategori));
	}
	
 
	public function ubah_kategori($data,$id_kategori)
	{
		$this->db->update("kategori",$data,$id_kategori);
	}
 
	public function hapus_kategori($id_kategori)
	{
		$this->db->where('id_kategori', $id_kategori);
		$this->db->delete('kategori'); 
	}


	///---------------------------------------------END KATEGORI-----------------------------------------------------------//

	public function get_merk()
	{
		return $this->db->query('SELECT * FROM merk 
								 JOIN kategori ON merk.id_kategori = kategori.id_kategori 
								 ORDER BY id_merk DESC');
		
	}

	public function ubah_merk($data,$id_merk){
		$this->db->update("merk",$data,$id_merk);
	}

	public function get_detail_merk($id_merk){
		

		return $this->db->query('SELECT * FROM merk 
								 JOIN kategori ON merk.id_kategori = kategori.id_kategori 
								 WHERE id_merk="'.$id_merk.'"');
	}


	public function hapus_merk($id_merk){
		$this->db->where('id_merk', $id_merk);
		$this->db->delete('merk'); 
	}
    ///---------------------------------------------END MERK-----------------------------------------------------------//


    //public function get_divisi(){
		//return $this->db->get('bagian');
	//}
	public function get_detail_bagian($kode_bagian){
		return $this->db->get_where('bagian', array('kode_bagian' => $kode_bagian));
	}
	
	public function ubah_divisi($data,$id_bagian){
		$this->db->update("bagian",$data,$id_bagian);
	}
	
	public function hapus_divisi($kode_bagian){
		$this->db->where('kode_bagian', $kode_bagian);
		$this->db->delete('bagian'); 
	}

	///---------------------------------------------END DIVISI-----------------------------------------------------------//

	function export_laporan(){
        $query = $this->db->query("SELECT * FROM barang
        						   JOIN kategori ON barang.kode_kategori = kategori.id_kategori
        						   JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
        						   JOIN merk ON barang.merk = merk.id_merk");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    ///---------------------------------------------END CETAK LAPORAN-----------------------------------------------------------//

    public function GetBarangGudang(){
		return $row = $this->db->query('SELECT * FROM barang 
								   JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								   JOIN merk ON barang.merk = merk.id_merk
								   WHERE kode_bagian="GDU" ORDER BY id_barang DESC');
		
	}


		
	
 
	public function update_barang($data,$kode_kategori){
		$this->db->update("kategori",$data,$kode_kategori);
	}
	public function delete_barang($kode_kategori){
		$this->db->where('kode_kategori', $kode_kategori);
		$this->db->delete('kategori'); 
	}
 
	

	public function get_divisi()
	{
		$this->db->select('*');
		$this->db->from('bagian');
		$bag = $this->db->get();
		return $bag->result();
	}



	public function update_bagian($data,$key)
	{
		$this->db->update("barang",$data,$key);
	}


	public function get_sidebarnew()
	{
		$this->db->query('SELECT kode_barang FROM barang WHERE NOT EXISTS 
	   							 (SELECT * FROM bagian WHERE barang.kode_bagian = bagian.kode_bagian)');
		$count = $this->db->get();
		return $count->num_rows(); 
	}


}