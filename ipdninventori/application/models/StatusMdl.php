<?php 
class StatusMdl extends CI_Model {

	function __construct() {
        parent::__construct();
      	$this->load->database();
    }

	public function GetBarangTersedia()
	{
		return $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.status = "Tersedia" ORDER BY id_barang DESC');

	}

	public function GetBarangTerpakai()
	{
		return $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.status = "Terpakai" ORDER BY id_barang DESC');

	}

	public function GetBarangRusak()
	{
		return $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.status = "Rusak" ORDER BY id_barang DESC');

	}

	public function GetBarangRusakTotal()
	{
		return $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								  JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.status = "Rusak Total" ORDER BY id_barang DESC');

	}

	public function GetBarangPerbaiki()
	{
		return $this->db->query('SELECT * FROM barang 
								 JOIN kategori ON barang.kode_kategori = kategori.id_kategori
								 JOIN bagian ON barang.kode_bagian = bagian.kode_bagian
								 JOIN merk ON barang.merk = merk.id_merk
								 WHERE barang.status = "Diperbaiki" ORDER BY id_barang DESC');

	}

	public function update_terpakai($id_barang)
	{
		return $this->db->query('UPDATE barang SET status="Terpakai" WHERE id_barang="'.$id_barang.'"');
	}

	public function update_tersedia($id_barang)
	{
		return $this->db->query('UPDATE barang set status="Tersedia" where id_barang="'.$id_barang.'"');
	}

	public function update_rusak($id_barang)
	{
		return $this->db->query('UPDATE barang set status="Rusak" where id_barang="'.$id_barang.'"');
	}

	public function update_perbaiki($id_barang)
	{
		return $this->db->query('UPDATE barang set status="Diperbaiki" where id_barang="'.$id_barang.'"');
	}

	public function update_rusak_total($id_barang)
	{
		return $this->db->query('UPDATE barang set status="Rusak Total" where id_barang="'.$id_barang.'"');
	}

	public function GetTotal(){
		return $this->db->query('select id_barang from barang');
		
	}

	public function GetLokasi()
	{
		return $this->db->query('SELECT kode_bagian FROM bagian');
	}


}