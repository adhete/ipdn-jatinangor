$(document).ready(function(){
    $("#loading").hide();
    $("#loading").ajaxStart(function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
    });
    
    $('#loading').ajaxComplete(function(){
      $(this).fadeOut('fast');
      $('#loading').hide();
    });   
});

function download_excel()
{
    urlnya = window.location.href;
    urlnya = urlnya.split('laporan');
    console.log(urlnya[1]);
    //window.location = 'Barang_masuk/download_laporan' + urlnya[1];
    //window.location = mysite + 'admin/dashboard/download_laporan' + urlnya[1];
}

function load_page(url, div)
{    
    $(div).empty();
    $(div).ajaxStart(function(){
        //$(div).html('<img src="'+mysite+'themes/images/loading.gif" />');
    });       
    //ajax_loading(div);
    $(div).load(mysite + url);
}

function set_active_menu(menuid)
{
    $(menuid).attr('class', 'active');
}

function load_page_set_link(url, div, activelink)
{        
    set_active_menu(activelink);            
    load_page(url, div);
}

function reset_menu_master()
{
    $("#sektor-menu").attr('class', '');
    $("#skala-menu").attr('class', '');
    $("#kecamatan-menu").attr('class', '');
    $("#kelurahan-menu").attr('class', '');    
}

function showConfirm(header, message, functionYes, functionNo)
{
    $("#mc-header").html(header);
    $("#mc-message").html(message);
    //$("#mc-yes").attr('onclick', functionYes);
    $( "#mc-yes").unbind( "click" );
    $("#mc-yes").on('click', (function() {
        functionYes();
    }));
    $("#myModalConfirm").modal('show');
}

function save_form(url, val_data, callback)
{
	var html = '<div id="load"></div>';
    $('#loading').append(html); 
    $('#loading').show();
    $.ajax({
        url: mysite + url, 
        type: "POST",             
        data: val_data, 
        contentType: false,       
        cache: false,           
        processData:false,       
        success: function(data)   
        {
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $('#myModal').modal('show');
            $('#loading').hide();

            if(obj.header.toLowerCase() == "sukses")
            {
            	callback();
            }
        }
    });
}

//////////////////////////////////////////////////////////////////////////////
function reset_skala()
{
    $("#id_skala").val(null);
    $("#nama_skala").val(null);
}

function edit_skala(a,b)
{
    $("#id_skala").val(a);
    $("#nama_skala").val(b);
}

function delete_skala(a, csrf)
{
	showConfirm("Konfirmasi", "Hapus Skala?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove",
        {
            __USAHABDG__: csrf,
            TABLE:'m_skala',
            TABLE_KEY:'id',
            KEY_VALUE:a
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/skala', "#my-properties", "#skala-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}
//////////////////////////////////////////////////////////////////////////////

function reset_sektor()
{
    $("#id_sektor").val(null);
    $("#nama_sektor").val(null);
}

function edit_sektor(a,b)
{
    $("#id_sektor").val(a);
    $("#nama_sektor").val(b);
}

function delete_sektor(a, csrf)
{
	showConfirm("Konfirmasi", "Hapus Sektor?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove",
        {
            __USAHABDG__: csrf,
            TABLE:'m_sektor',
            TABLE_KEY:'id',
            KEY_VALUE:a
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/sektor', "#my-properties", "#sektor-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

//////////////////////////////////////////////////////////////////////////////

function reset_kecamatan()
{
    $("#id_kecamatan").val(null);
    $("#nama_kecamatan").val(null);
}

function edit_kecamatan(a,b)
{
    $("#id_kecamatan").val(a);
    $("#nama_kecamatan").val(b);
}

function delete_kecamatan(a, csrf)
{
	showConfirm("Konfirmasi", "Hapus Kecamatan?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove",
        {
            __USAHABDG__: csrf,
            TABLE:'m_kecamatan',
            TABLE_KEY:'id',
            KEY_VALUE:a
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/kecamatan', "#my-properties", "#kecamatan-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

////////////////////////////////////////////////////////////////////////////////////////////

function reset_kelurahan()
{
	$("#id_kecamatan").val(null);
    $("#id_kelurahan").val(null);
    $("#nama_kelurahan").val(null);
}

function edit_kelurahan(a,b,c)
{
    $("#id_kelurahan").val(a);
    $("#id_kecamatan").val(b);
    $("#nama_kelurahan").val(c);
}

function delete_kelurahan(a, csrf)
{
	showConfirm("Konfirmasi", "Hapus Kelurahan?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove",
        {
            __USAHABDG__: csrf,
            TABLE:'m_kelurahan',
            TABLE_KEY:'id',
            KEY_VALUE:a
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/kelurahan', "#my-properties", "#kelurahan-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function reset_menu_pnu()
{
	$("#app-usaha-menu").attr('class','');
	$("#app-pemilik-menu").attr('class','');
	$("#usaha-menu").attr('class','');
	$("#pemilik-menu").attr('class','');
}


$(document).ready(function(e){
    $('#listadmin').DataTable();
    $('#listlapush').DataTable();
	if(master) {
		load_page_set_link('admin/dashboard/skala', "#my-properties", "#skala-menu");
	}

	if(pnu) {
        if(app_p) {
            load_page_set_link('admin/dashboard/approve_pemilik', "#my-properties", "#app-pemilik-menu");
        } else if(usaha_menu) {
            load_page_set_link('admin/dashboard/usaha', "#my-properties", "#usaha-menu");
        } else {
            load_page_set_link('admin/dashboard/approve_usaha', "#my-properties", "#app-usaha-menu");
        }
	}


    $("#form-edit-usaha").on('submit', (function(e) {       
        data = new FormData(this);
        //data = data.replace("%3B", "-")
        e.preventDefault();
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.ajax({
            url: mysite + "admin/dashboard/save_edit", 
            type: "POST",             
            data: data, 
            contentType: false,       
            cache: false,           
            processData:false,       
            success: function(data)   
            {
                var obj = JSON.parse(data);
                $('#load').remove();

                $("#m-header").html(obj.header);
                $("#m-message").html(obj.message);
                $("#myModal").modal('show');

                if(obj.header.toLowerCase() == "sukses") {
                    
                    window.location = mysite + "admin/dashboard/pemilik_usaha?usaha";
                    
                }

                $('#loading').hide();
            }
        });
    }));

    $("#filegambar").fileinput({
        maxFilePreviewSize: 300,
        maxFileSize: 300,
        minFileCount: 1,
        maxFileCount: 5,
        showRemove:false,
        showUpload:false,
        allowedFileExtensions: ["jpg", "jpeg", "png"]
    });

    $("#form-submit-usaha").on('submit',(function(e) {
        data = new FormData(this);
        var ins = document.getElementById('filegambar').files.length;
        for (var x = 0; x < ins; x++) {
            data.append("filegambar[]", document.getElementById('filegambar').files[x]);
        }
        console.log(data);

        e.preventDefault();
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.ajax({
            url: mysite + "admin/dashboard/save_usaha", 
            type: "POST",             
            data: data, 
            contentType: false,       
            cache: false,           
            processData:false,       
            success: function(data)   
            {
                var obj = JSON.parse(data);
                $('#load').remove();

                $("#m-header").html(obj.header);
                $("#m-message").html(obj.message);
                $("#myModal").modal('show');

                if(obj.header.toLowerCase() == "sukses") {
                    
                    window.location = mysite + "admin/dashboard/pemilik_usaha?usaha";
                    
                }

                $('#loading').hide();
            },
            error: function(e) {
                $("#m-header").html("Terdapat Kesalahan");
                $("#m-message").html("Terdapat Kesalahan Saat Menyimpan");
                $("#myModal").modal('show');
            }

        });
    }));

});

function update_pemilik()
{
    var data = $("#update-profile").serializeArray();
    console.log(data);
    var html = '<div id="load"></div>';
    $('#loading').append(html); 
    $('#loading').show();
    $.post(mysite + "admin/dashboard/save", 
        $.param(data), 
        function(data) {
            var obj = JSON.parse(data);
            $('#load').remove();

            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');

            if(obj.header.toLowerCase() == "sukses") {
                
                load_page_set_link('admin/dashboard/pemilik', "#my-properties", "#pemilik-menu");
                
            }

            $('#loading').hide()
    });
}

function simpan_admin()
{
    var data = $("#form-tbh-admin").serializeArray();
    console.log(data);
    var html = '<div id="load"></div>';
    $('#loading').append(html); 
    $('#loading').show();
    $.post(mysite + "admin/dashboard/save", 
        $.param(data), 
        function(data) {
            var obj = JSON.parse(data);
            $('#load').remove();

            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');

            if(obj.header.toLowerCase() == "sukses") {
                
                location.reload();
                
            }

            $('#loading').hide()
    });
}

function detail_usaha(id)
{
    load_page_set_link('admin/dashboard/detail_usaha/'+id, "#my-properties", "#app-usaha-menu");
}

function edit_usaha(id)
{
    window.location = mysite + 'admin/dashboard/edit_usaha/'+id;

    //load_page_set_link('admin/dashboard/edit_usaha/'+id, "#my-properties", "#usaha-menu");
}

function detail_usaha2(id)
{
    load_page_set_link('admin/dashboard/detail_usaha/'+id, "#my-properties", "#usaha-menu");
}

function hapus_usaha(id, csrf)
{
    showConfirm("Konfirmasi", "Hapus Usaha?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove_status",
        {
            __USAHABDG__: csrf,
            TABLE:'t_usaha',
            TABLE_KEY:'id',
            id:id,
            di_hapus:"Y",
            status_aktif:'T'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/usaha', "#my-properties", "#usaha-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function edit_admin(id)
{
    $.getJSON(mysite + 'admin/dashboard/get_admin/'+id, function(data) {
        for (var i in data) {
            $('input[name="'+i+'"]').val(data[i]);

            if(i == "alamat") {
                $("#alamat").html(data[i]);
            }

            if(i == "jk") {
                if(data[i] == "L") {
                    console.log('laki');
                    $("#form-laki label div.iradio").attr("class","iradio checked");
                    $("#form-cewe label div.iradio").attr("class","iradio");
                } else {
                    console.log('cewe');
                    $("#form-laki label div.iradio").attr("class","iradio");
                    $("#form-cewe label div.iradio").attr("class","iradio checked");
                }
            }

            if(i == "hak_akses") {
                if(data[i] == "ADMIN") {
                    $("#form-hadmin label div.iradio").attr("class","iradio checked");
                    $("#form-hsuper label div.iradio").attr("class","iradio");
                } else {
                    $("#form-hadmin label div.iradio").attr("class","iradio");
                    $("#form-hsuper label div.iradio").attr("class","iradio checked");
                }
            }

        }
    });
}

function delete_admin(id, csrf)
{
    showConfirm("Konfirmasi", "Hapus Admin?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove",
        {
            __USAHABDG__: csrf,
            TABLE:'m_admin',
            TABLE_KEY:'id',
            KEY_VALUE:id,
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            location.reload();
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function undo_usaha(id, csrf)
{
    showConfirm("Konfirmasi", "Undo Hapus Usaha?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove_status",
        {
            __USAHABDG__: csrf,
            TABLE:'t_usaha',
            TABLE_KEY:'id',
            id:id,
            di_hapus:"T",
            status_aktif:'Y'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/usaha', "#my-properties", "#usaha-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function hapus_pemilik(id, csrf)
{
    showConfirm("Konfirmasi", "Dengan menghapus pemilik, maka usaha yang bersangkutan akan otomatis tidak aktif. Lanjutkan?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove_status",
        {
            __USAHABDG__: csrf,
            TABLE:'t_pemilik',
            TABLE_KEY:'id',
            id:id,
            di_hapus:"Y",
            status_aktif:'T'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/pemilik', "#my-properties", "#pemilik-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function undo_pemilik(id, csrf)
{
    showConfirm("Konfirmasi", "Dengan meng-undo pemilik, maka usaha yang bersangkutan akan otomatis aktif kembali. Lanjutkan?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/remove_status",
        {
            __USAHABDG__: csrf,
            TABLE:'t_pemilik',
            TABLE_KEY:'id',
            id:id,
            di_hapus:"T",
            status_aktif:'Y'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/pemilik', "#my-properties", "#pemilik-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function goTambahUsaha()
{
    window.location = mysite + 'admin/dashboard/tambah_usaha';
}

function goTambahPemilik()
{
    load_page_set_link('admin/dashboard/tambah_pemilik', "#my-properties", "#pemilik-menu");
}

function showmap(_latitude,_longitude)
{
    
}

function approve_usaha(id, csrf)
{
    showConfirm("Konfirmasi", "Approve Usaha?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/save_approve",
        {
            __USAHABDG__: csrf,
            id:id,
            TABLE:'t_usaha',
            TABLE_KEY:'id'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/approve_usaha', "#my-properties", "#app-usaha-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });
}

function detail_pemilik(id)
{
    load_page_set_link('admin/dashboard/detail_pemilik/'+id, '#my-properties', '#app-pemilik-menu');
}

function detail_pemilik2(id)
{
    load_page_set_link('admin/dashboard/detail_pemilik/'+id, '#my-properties', '#pemilik-menu');
}

function edit_pemilik(id)
{
    load_page_set_link('admin/dashboard/detail_pemilik/'+id+"?edit", '#my-properties', '#pemilik-menu');
}


function approve_pemilik(id, csrf, aktif_email)
{
    if(aktif_email=="T")
    {
        $("#m-header").html("Gagal Approve");
        $("#m-message").html("Pemilik Belum Verifikasi Email-nya, Approve bisa dilakukan jika pemilik sudah men-verifikasi Emailnya.");
        $("#myModal").modal('show');
        return;
    }

    showConfirm("Konfirmasi", "Approve Pemilik?", function(){
        var html = '<div id="load"></div>';
        $('#loading').append(html); 
        $('#loading').show();
        $.post(mysite + "admin/dashboard/save_approve",
        {
            __USAHABDG__: csrf,
            id:id,
            TABLE:'t_pemilik',
            TABLE_KEY:'id'
        },
        function(data, status){
            var obj = JSON.parse(data);
            $('#load').remove();
            $("#m-header").html(obj.header);
            $("#m-message").html(obj.message);
            $("#myModal").modal('show');
            $('#loading').hide();
            load_page_set_link('admin/dashboard/approve_pemilik', "#my-properties", "#app-pemilik-menu");
        });
    }, function(){
        console.log('Rejected By User');
    });


}